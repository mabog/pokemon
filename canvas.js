class Canvas {
    myCanvas = (img,x, y) => {
        let c = document.getElementById('canvas');
        let context = c.getContext("2d");
        img.onload = () => {
            context.drawImage(img,x, y);
        }
        
    }
    
    resetCanvas = () => {
        let c = document.getElementById('canvas');
        let context = c.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height) // until play again btn
    }
}

export {Canvas};