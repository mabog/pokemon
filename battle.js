
class Battle {
    app;
    canvas;
    constructor(app, canvas) {
        this.app = app;
        this.canvas = canvas;
    }
    makeRandomID  = (attackerID) => {
        const targetID = Math.floor(Math.random() * 20 );
        if(targetID!=attackerID) {
            return targetID;
        }
        makeRandomID(attackerID);
    }

    start = async (attackerID) => {
        this.toggleReplayButton();
        this.canvas.resetCanvas();
        
        //load pokemons
        // const me = new Pokemon(await (this.app.getPokemons(myPokemons[attackerID].url)));
        const randomPokemon = myPokemons[this.makeRandomID(attackerID)].url;
        const target = new Pokemon(await (this.app.getPokemons(randomPokemon)));
        let first, second;
        
        if(me.Speed > target.Speed) {
            first = me;
            first.downPosition=true;
            second = target;
            second.downPosition=false;
        }  
        else {
            first = target;
            first.downPosition=false;
            second = me;
            second.downPosition=true;
        }

        //draw initial positions & HPbars

        me.drawInitial()
        target.drawInitial();
        updateHPs(first, second);

        const match = (first, second) => {
            // match loop
            while((first.hp > 0) && (second.hp > 0)) {
                let rng = Math.random()*200;
                let dmg = Math.floor((first.Attack / second.Defense)*rng);
                if(dmg > 0) {
                    attack(first, second, dmg)
                    if((first.hp > 0) && (second.hp > 0)) swapPlaces();
                }
            }
        }

        //Delay before match start
        setTimeout(match, 500, first, second);

        // const res = await match(first, second);

        // Assign winner
        if(first.hp > 0) {
            winner = first;
        }
        else {
            winner = second;
        }

        // Determine winner
        if(me.Name==winner.Name) {
            displayResult('you win')
        }
        else displayResult('you lose');
        
        this.toggleReplayButton();
    }

    toggleReplayButton = () => {
        let replayBtn = document.getElementById('replayBtn');
        if (replayBtn.style.display === "none") {
            replayBtn.style.display = "block";
        } else {
            replayBtn.style.display = "none";
        }
    }

    displayResult = (result) => {
        document.getElementById('result').innerHTML = result;
    }

    updateHPs = (...pokemons) => {
        pokemons.forEach(pokemon => {
            if(pokemon.downPosition===true) {
                document.getElementById('meHP').innerHTML = `HP ${pokemon.hp}`;
            }
            else
                document.getElementById('targetHP').innerHTML = `HP ${pokemon.hp}`;
        });
    }

    // Attack
    attack = (first, second, dmg) => {
        first.hide();
        first.moveToBattlefield()    

        second.blink();
        second.blink();
        second.blink();  

        first.hide();
        first.drawInitial();

        second.hp-= dmg;
        if(second.hp < 0) second.hp=0;
        updateHPs(first, second);
    }

    // swap players to make turns
    swapPlaces = (first, second) => {
        temp=first;
        first=second;
        second=temp;
    }
}

export {Battle};