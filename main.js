import { Pokemon } from './pokemon.js'
import {Canvas} from './canvas.js'
import {Battle} from './battle.js'

class App {
    
    pokemons;
    getPokemons = async (URL) => {
        try {
            const result = await fetch(URL).then((res) => res.json())
        return result;

        } catch (error) {
            console.log(error);
        }
    };

    displayAll = async () => {
        const baseURL ='https://pokeapi.co/api/v2/pokemon/';
        this.pokemons = await this.getPokemons(baseURL).then(res => res.results);
        
        for (const [index] of this.pokemons.entries()) {
            await this.getPokemons(this.pokemons[index].url).then(async (result) => {
                let pokemonDiv = document.createElement("DIV");
                const pokemon = new Pokemon(result, canvas)
                // console.log(pokemon);//////////////////
                pokemon.displayPokemonInfo(pokemon, pokemonDiv, index);
                document.getElementById("container").appendChild(pokemonDiv);
            })
        }
    };

}

const app = new App();
const canvas = new Canvas(); // unused
app.displayAll();

const battle = new Battle(app, canvas);

document.addEventListener('click',function(e){
    if(e.target){
        

        const attackerID =- e.target.id;
        const me = this.pokemons[attackerID];
        console.log(me)////////
        battle.start();
    }
    if(e.target && e.target.id== 'replayBtn'){
        resetCanvas();
    }
});
