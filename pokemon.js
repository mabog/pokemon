import {Canvas} from './canvas.js'

class Pokemon {   
    Name;
    Ability;
    Move1;
    Move2;
    Move3;
    Move4;
    moves;
    Speed;
    SpecialDefense;
    SpecialAttack;
    Defense;
    Attack;
    hp;
    frontSprite
    backSprite;
    downPosition;
    canvas
    constructor(pokemon, canvas) {
        this.canvas = canvas;
        this.Name= pokemon.name;
        this.Ability = this.getVisibleAbility(pokemon.abilities);
        this.moves=pokemon.moves;
        this.Move1= pokemon.moves[0].move.name;
        this.Move2= pokemon.moves[1].move.name;
        this.Move3= pokemon.moves[2].move.name;
        this.Move4= pokemon.moves[3].move.name;
        this.Speed= pokemon.stats[5].base_stat;
        this.SpecialDefense= pokemon.stats[4].base_stat;
        this.SpecialAttack= pokemon.stats[3].base_stat;
        this.Defense= pokemon.stats[2].base_stat;
        this.Attack= pokemon.stats[1].base_stat;
        this.hp= pokemon.stats[0].base_stat
        this.frontSprite=pokemon.sprites.front_default;
        this.backSprite=pokemon.sprites.back_default
    }

    getVisibleAbility = (abilities) => {
        for (const ability of abilities) {
            if(ability.is_hidden===false) return ability.ability.name;
        }
    }

    drawInitial = () => {
        let img = document.createElement("img")
        if(this.downPosition==true){
            //draw attacker
            img.src = this.backSprite;
            this.myCanvas(img, 0, 2*96);
        }
        else {
            //draw target
            img.src = this.frontSprite;
            this.myCanvas(img, 0, 0);
        }
    }

    getCorrectSprite = () => {
        let img = document.createElement("img")  
        if(this.downPosition==true){
            //draw attacker
            img.src = this.backSprite;
            return img;
        }
        else {
            //draw target
            img.src = this.frontSprite;
            return img;
        }
    }

    getCorrentY = () => {
        if(this.downPosition==true){
            //draw attacker
            return 2*96;
        }
        else {
            //draw target
            return 0;
        }
    }
    
    moveToBattlefield = () => {
        this.myCanvas(this.getCorrectSprite(), 0, 96);
    }
    
    hide = () => {
        let c = document.getElementById('canvas');
        let context = c.getContext("2d");

        context.globalCompositeOperation ="xor";   
        let img = this.getCorrectSprite();
        img.width=0;
        img.height=this.getCorrentY();
        img.onload = () => {
            context.drawImage(img, img.width, img.height)
        }
    }

    blink = () => {
            this.hide()
            setTimeout(this.drawInitial, 500);
    }
    
    addLine = (line, pokemonDiv) => {
        let p = document.createElement("p");
        let t = document.createTextNode(line);
        p.appendChild(t);
        pokemonDiv.appendChild(p);
    }

    displayPokemonInfo = (pokemon, pokemonDiv, index) => {
        let img = document.createElement('img');
        img.src=pokemon.frontSprite;
        img.setAttribute('id', index);
        // img.addEventListener('click', (event) => {battle(event.path[0].id)});
        pokemonDiv.appendChild(img);

        this.addLine('Name: '+ pokemon.Name, pokemonDiv);
        this.addLine(' Ability: '+ pokemon.Ability, pokemonDiv);
        this.addLine(' Move 1: '+ pokemon.Move1, pokemonDiv);
        this.addLine(' Move 2: '+ pokemon.Move2, pokemonDiv);
        this.addLine(' Move 3: '+ pokemon.Move3, pokemonDiv);
        this.addLine(' Move 4: '+ pokemon.Move4, pokemonDiv);
        this.addLine(' Speed: '+ pokemon.Speed, pokemonDiv);
        this.addLine('Special Defense: ' + pokemon.SpecialDefense, pokemonDiv);
        this.addLine('Special Attack: ' + pokemon.SpecialAttack, pokemonDiv);
        this.addLine('Defense: ' + pokemon.Defense, pokemonDiv);
        this.addLine('Attack: ' + pokemon.Attack, pokemonDiv);
        this.addLine('HP: ' + pokemon.hp, pokemonDiv);
    }

};

export {Pokemon};